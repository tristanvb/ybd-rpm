import os
from cache import cache_key, get_cache
from app import log, timer
import app
import sandbox
import shutil
import yaml

# Because rpm is otherwise totally broken
#
common_rpm_args  = '--dbpath=/var/lib/rpm '
common_rpm_args += '--define "_rpmconfigdir /usr/lib/rpm" '
common_rpm_args += '--define "_rpmlock_path /var/lib/rpm/.rpm.lock" '
common_rpm_args += '--define "_fileattrsdir /usr/lib/rpm/fileattrs" '
common_rpm_args += '--define "_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm" '
common_rpm_args += '--define "_rpmfilename %{_build_name_fmt}" '
common_rpm_args += '--define "_tmppath /tmp" '
common_rpm_args += '--define "__spec_clean_cmd echo" '

# foreach_def
# @dn: The target definition or name
# @callback: The callback to call
# @user_data: Something to be passed to @callback along with the definition
#
# Iterates over each chunk which is to be included
# in the target @dn in order of dependency, the passed
# @dn definition will be the last one called.
#
def foreach_def(dn, callback, user_data, traversed={}):

    if type(dn) is not dict:
        dn = app.defs.get(dn)

    # if we can't calculate cache key, we can't create this component
    if cache_key(dn) is False:
        if 'tried' not in dn:
            log(dn, 'No cache_key, so skipping compose')
            dn['tried'] = True
        return False

    # if dn is already cached, we're done
    if not get_cache(dn):
        log('SMTH', '%s is not cached, try again sucker !' % dn['name'], exit=True)

    systems = dn.get('systems', [])
    for system in systems:
        for s in system.get('subsystems', []):
            subsystem = app.defs.get(s['path'])
            foreach_def(subsystem, callback, user_data, traversed)
        foreach_def(system['path'], callback, user_data, traversed)


    contents = dn.get('contents', [])
    for it in contents:
        item = app.defs.get(it)

        if traversed.get (item.get ('name')):
            continue

        if item.get('build-mode', 'staging') != 'bootstrap':

            if not traversed.get (item.get ('name')):
                foreach_def(item, callback, user_data, traversed)

    callback (dn, user_data)
    traversed[dn.get('name')]=True


def compose_file_list (stage_dir, components):
    file_list=[]

    for filename in components:

        # XXX Use shlex.quote to quote the filenames !
                    
        # Omit directories for now, rpm packages owning directories
        # is a can of worms. Also omit the baserock directory as
        # it just gets in the way.
        testfile = os.path.join(stage_dir, filename)
        if not os.path.isdir(testfile) and filename != 'baserock':
            file_list.append ('"/%s"\n' % filename)

    return file_list

def generate_spec (stage_dir, metafile, output, name):
    with open(metafile, "r") as metafile_f:
        with open(output, "w") as output_f:
            metadata = yaml.safe_load(metafile_f)
            description = 'No Description'
            
            if metadata.get('description') is not None:
                description = metadata.get('description')
            
            # Write out the package header first
            output_f.write ('Name: %s\n' % name);
            output_f.write ('Summary: %s\n' % description);
            output_f.write ('Version: %s\n' % metadata.get('ref'))
            output_f.write ('Release: 1\n')
            output_f.write ('License: %s\n' % 'Undetermined')
            output_f.write ('\n');
            
            output_f.write ('%%description\n')
            output_f.write ('%s\n' % description)
            output_f.write ('\n')

            # XXX Work around ybd metadata bug
            product_seen={}

            for product in metadata['products']:

                # XXX Work around ybd metadata bug
                if product_seen.get (product['artifact']):
                    continue
                product_seen[product['artifact']] = True

                file_list = compose_file_list(stage_dir, product['components'])

                # Dont bother creating packages with no payload
                if len(file_list) == 0:
                    continue
                
                # Sub-Package header
                output_f.write ('%%package -n %s\n' % product['artifact'])
                output_f.write ('Summary: %s\n' % description);
                output_f.write ('\n');

                output_f.write ('%%description -n %s\n' % product['artifact']);
                output_f.write ('%s\n' % description)
                output_f.write ('\n');

                # Sub-Package files
                output_f.write ('%%files -n %s\n' % product['artifact'])
                for filename in file_list:
                    output_f.write (filename)
                output_f.write ('\n');


def package_one_rpm (dn, system):

    kind = dn.get ('kind')
    name = dn.get ('name')

    if kind == 'chunk' or kind is None:

        with timer (name):
            subdir      = '%s.inst' % name
            fulldir     = os.path.join (system['sandbox'], subdir)
            metadir     = os.path.join (system['sandbox'], '%s.meta' % name)
            baserockdir = os.path.join (fulldir, 'baserock')
            
            # Install the chunk we're gonna package under subdir
            sandbox.install (system, dn, subdir)

            # Move the baserock directory out of the way, we dont package the metadata
            shutil.move (baserockdir, metadir)

            # Generate the specfile in the metadir, note that we use
            # the metadata for the given package from the system metadata
            # directory, not the metadata for the specific chunk artifact

            # XXX Right now the chunk's individual metadata is richer, it includes
            # the desciption, change this to use the system metadata for that chunk
            # later !!!
            #metafile = os.path.join (system['sandbox'], 'baserock', '%s.meta' % name)
            metafile = os.path.join (metadir, '%s.meta' % name)
            specfile = os.path.join (metadir, '%s.spec' % name)

            generate_spec (fulldir, metafile, specfile, name)

            # XXX Now we gonna run rpmbuild in the sandbox !!!
            #
            command='rpmbuild ' + common_rpm_args + ' --buildroot=/%s.inst --define "_rpmdir /RPMS" -bb /%s.meta/%s.spec' % (name, name, name)
            env_vars = sandbox.env_vars_for_build (system)
            sandbox.run_sandboxed(system, command, env_vars)
            
            app.log(dn, "Removing sandbox dir", fulldir, verbose=True)
            shutil.rmtree (fulldir)
            shutil.rmtree (metadir)

# package_rpms
# @system: The system to package rpms for
#
# This function will first stage the given @system, which
# must have an rpm installation, and then it will use the
# metadata in the system's baserock directory to package
# each individual chunk, by staging those chunks one by one
# and packaging them in a chroot.
#
# Care will be taken to build the rpms in the order of their
# dependencies, this should allow rpm to infer package dependencies
# correctly
#
def package_rpms (system):

    if type(system) is not dict:
        system = app.defs.get(system)

    system_kind = system.get ('kind')
    if system_kind != 'system':
        log('RPM', '%s is not a system, cannot package rpms !' % system.get ('name'), exit=True)

    with sandbox.setup(system):
        install_contents(system)

        # First initialize the db
        command='rpm ' + common_rpm_args + ' --initdb'
        env_vars = sandbox.env_vars_for_build (system)
        sandbox.run_sandboxed(system, 'mkdir /var/lib/rpm', env_vars)
        sandbox.run_sandboxed(system, 'mkdir /RPMS', env_vars)
        sandbox.run_sandboxed(system, command, env_vars)

        # Package each rpm in order of build dependency
        foreach_def (system, package_one_rpm, system)

        # Move the resulting RPMS directory into the deployment area
        rpmdir = os.path.join (system['sandbox'], 'RPMS')
        shutil.move (rpmdir, app.config['deployment'])
        
#
# XXX Stuff taken from assembly.py in ybd
#
def install_contents(dn, contents=None):
    ''' Install contents (recursively) into dn['sandbox'] '''

    if contents is None:
        contents = dn.get('contents', [])

    log(dn, 'Installing contents\n', contents, verbose=True)

    for it in contents:
        item = app.defs.get(it)
        if os.path.exists(os.path.join(dn['sandbox'],
                                       'baserock', item['name'] + '.meta')):
            # content has already been installed
            log(dn, 'Already installed', item['name'], verbose=True)
            continue

        for i in item.get('contents', []):
            install_contents(dn, [i])

        if item.get('build-mode', 'staging') != 'bootstrap':
            if not get_cache(item):
                log('RPM', '%s is not cached, cannot stage the system !' % item.get ('name'), exit=True)
            sandbox.install(dn, item)
