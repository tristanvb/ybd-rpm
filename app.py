import contextlib
import os
import datetime
from repos import get_version
import sys
import warnings
import yaml
import shutil

# XXX Do we really need this ? We're not multiprocessing...
from multiprocessing import Value, Lock


config = {}
defs = {}


def log(dn, message='', data='', verbose=False, exit=False):
    ''' Print a timestamped log. '''

    if exit:
        print('\n\n')
        message = 'ERROR: ' + message.replace('WARNING: ', '')

    if verbose is True and config.get('log-verbose', False) is False:
        return

    name = dn['name'] if type(dn) is dict else dn

    timestamp = datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S ')
    if config.get('log-timings') == 'elapsed':
        timestamp = timestamp[:9] + elapsed(config['start-time']) + ' '
    if config.get('log-timings', 'omit') == 'omit':
        timestamp = ''
    progress = ''
    if config.get('counter'):
        count = config['counter'].get()
        progress = '[%s/%s/%s] ' % (count, config['tasks'], config['total'])
    entry = '%s%s[%s] %s %s\n' % (timestamp, progress, name, message, data)
    if config.get('instances'):
        entry = str(config.get('fork', 0)) + ' ' + entry

    print(entry),
    sys.stdout.flush()

    if exit:
        print('\n\n')
        os._exit(1)


def log_env(log, env, message=''):
    with open(log, "a") as logfile:
        for key in sorted(env):
            msg = env[key] if 'PASSWORD' not in key else '(hidden)'
            logfile.write('%s=%s\n' % (key, msg))
        logfile.write(message + '\n\n')
        logfile.flush()


def warning_handler(message, category, filename, lineno, file=None, line=None):
    '''Output messages from warnings.warn() - default output is a bit ugly.'''

    return 'WARNING: %s\n' % (message)

def elapsed(starttime):
    td = datetime.datetime.now() - starttime
    hours, remainder = divmod(int(td.total_seconds()), 60*60)
    minutes, seconds = divmod(remainder, 60)
    return "%02d:%02d:%02d" % (hours, minutes, seconds)

# Code taken from Eli Bendersky's example at
# http://eli.thegreenplace.net/2012/01/04/shared-counter-with-pythons-multiprocessing
class Counter(object):
    def __init__(self, initval=0):
        self.val = Value('i', initval)
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1

    def get(self):
        with self.lock:
            return self.val.value


def setup(target, arch, ybddir, original_cwd=""):
    os.environ['LANG'] = 'en_US.UTF-8'
    config['ybddir'] = ybddir
    config['start-time'] = datetime.datetime.now()
    config['program'] = 'ybd-rpm.py'
    config['my-version'] = get_version(os.path.dirname(__file__))
    log('SETUP', '%s version is' % config['program'], config['my-version'])

    log('SETUP', 'Running %s in' % config['program'], os.getcwd())
    config['target'] = os.path.basename(os.path.splitext(target)[0])
    config['arch'] = arch
    config['sandboxes'] = []
    config['overlaps'] = []
    config['new-overlaps'] = []

    warnings.formatwarning = warning_handler
    # Suppress multiple instances of the same warning.
    warnings.simplefilter('once', append=True)

    # dump any applicable environment variables into a config file
    with open('./ybd.environment', 'w') as f:
        for key in os.environ:
            if key[:4] == "YBD_":
                f.write(key[4:] + ": " + os.environ.get(key) + '\n')
                
    # load config files in reverse order of precedence
    load_configs([
        os.path.join(os.getcwd(), 'ybd.environment'),
        os.path.join(os.getcwd(), 'ybd.conf'),
        os.path.join(original_cwd, 'ybd.conf'),
        os.path.join(ybddir, '..', 'ybd.conf'),
        os.path.join(ybddir, 'config', 'ybd.conf')])

    if not os.geteuid() == 0 and config.get('mode') == 'normal':
        log('SETUP', '%s needs root permissions' % sys.argv[0], exit=True)

    config['total'] = config['tasks'] = config['counter'] = 0
    config['systems'] = config['strata'] = config['chunks'] = 0
    config['reproduced'] = []
    config['keys'] = []
    config['pid'] = os.getpid()
    config['def-version'] = get_version('.')

    config['defdir'] = os.getcwd()
    config['extsdir'] = os.path.join(config['defdir'], 'extensions')
    if config.get('manifest') is True:
        config['manifest'] = os.path.join(config['defdir'],
                                          os.path.basename(config['target']) +
                                          '.manifest')
        try:
            os.remove(config['manifest'])
        except OSError:
            pass

    base_dir = os.environ.get('XDG_CACHE_HOME') or os.path.expanduser('~')
    config.setdefault('base',
                      os.path.join(base_dir, config['directories']['base']))
    for directory, path in config.get('directories', {}).items():
        try:
            if config.get(directory) is None:
                if path is None:
                    path = os.path.join(config.get('base', '/'), directory)
                config[directory] = path
            os.makedirs(config[directory])
        except OSError:
            if not os.path.isdir(config[directory]):
                log('SETUP', 'Cannot find or create', config[directory],
                    exit=True)

        log('SETUP', '%s is directory for' % config[directory], directory)

    # git replace means we can't trust that just the sha1 of a branch
    # is enough to say what it contains, so we turn it off by setting
    # the right flag in an environment variable.
    os.environ['GIT_NO_REPLACE_OBJECTS'] = '1'

    config['pid'] = os.getpid()
    config['counter'] = Counter()
    config['max-jobs'] = 1

def load_configs(config_files):
    for config_file in reversed(config_files):
        if os.path.exists(config_file):
            with open(config_file) as f:
                text = f.read()
                if yaml.safe_load(text) is None:
                    return
            log('SETUP', 'Setting config from %s:' % config_file)

            for key, value in yaml.safe_load(text).items():
                config[key.replace('_', '-')] = value
                msg = value if 'PASSWORD' not in key.upper() else '(hidden)'
                print '   %s=%s' % (key.replace('_', '-'), msg)
        print


def remove_dir(tmpdir):
    if (os.path.dirname(tmpdir) == config['tmp']) and os.path.isdir(tmpdir):
        try:
            print 'Removing %s' % tmpdir
            shutil.rmtree(tmpdir)
        except Exception, e:
            log('SETUP', 'WARNING: unable to remove %s' % tmpdir, str (e))

@contextlib.contextmanager
def chdir(dirname=None):
    currentdir = os.getcwd()
    try:
        if dirname is not None:
            os.chdir(dirname)
        yield
    finally:
        os.chdir(currentdir)

@contextlib.contextmanager
def timer(dn, message=''):
    starttime = datetime.datetime.now()
    log(dn, 'Starting ' + message)
    if type(dn) is dict:
        dn['start-time'] = starttime
    try:
        yield
    except:
        raise
    text = '' if message == '' else ' for ' + message
    time_elapsed = elapsed(starttime)
    log(dn, 'Elapsed time' + text, time_elapsed)
