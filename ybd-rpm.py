#!/usr/bin/python

import argparse
import app
from app import setup, timer, config, log
import os
from pots import Pots
import cache
from cache import cache_key, get_cache
import sandboxlib
import sandbox
from rpm import package_rpms

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument ('-a', dest='arch', required=True,
                         help="The build architecture")
    parser.add_argument ('-t', dest='target', required=True,
                         help="The build target")
    parser.add_argument ('-y', dest='ybddir', required=True,
                         help="The ybd directory")
    args = parser.parse_args()

    original_cwd = os.getcwd()
    if not os.path.exists('./VERSION'):
        if os.path.basename(os.getcwd()) != 'definitions':
            if os.path.isdir(os.path.join(os.getcwd(), 'definitions')):
                os.chdir(os.path.join(os.getcwd(), 'definitions'))
            else:
                if os.path.isdir(os.path.join(os.getcwd(), '..', 'definitions')):
                    os.chdir(os.path.join(os.getcwd(), '..', 'definitions'))

    setup (args.target, args.arch, args.ybddir, original_cwd)

    rpmdir = os.path.join (app.config['deployment'], 'RPMS')
    if os.path.exists (rpmdir):
        log('RPMS', 'Deployment directory already exists at:', rpmdir, exit=True)

    with timer('TOTAL'):

        target = os.path.join(config['defdir'], config['target'])
        log('TARGET', 'Target is %s' % target, config['arch'])
        with timer('DEFINITIONS', 'parsing %s' % config['def-version']):
            app.defs = Pots()

        target = app.defs.get(config['target'])
        if config.get('mode', 'normal') == 'parse-only':
            Pipeline(target)
            os._exit(0)

        with timer('CACHE-KEYS', 'cache-key calculations'):
            cache.cache_key(target)

        if config['total'] == 0 or (config['total'] == 1 and
                                    target.get('kind') == 'cluster'):
            log('ARCH', 'No definitions for', config['arch'], exit=True)

        app.defs.save_trees()

        sandbox.executor = sandboxlib.executor_for_platform()
        log(config['target'], 'Sandbox using %s' % sandbox.executor)
        if sandboxlib.chroot == sandbox.executor:
            log(config['target'], 'WARNING: using chroot is less safe ' +
                'than using linux-user-chroot')

        with timer('RPMS'):
            package_rpms (target)
    
    #genspec.generate_spec (args.artifact_dir, args.metafile, args.output, args.name)
